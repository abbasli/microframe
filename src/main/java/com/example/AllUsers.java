package com.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AllUsers {

    private List<User> myList = new ArrayList<>();

    public void addUser(User user) {
        myList.add(user);
    }

    public User checkUser(User user) {
        for (User realUser : myList) {
            if (Objects.equals(user.getUsername(), realUser.getUsername()) && Objects.equals(user.getPassword(), realUser.getPassword())) {
                return realUser;
            }
        }
        return null;
    }
}
