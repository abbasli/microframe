package com.example;

import lombok.Data;
import org.apache.catalina.LifecycleException;
import org.springframework.http.server.reactive.HttpHandler;
import org.springframework.http.server.reactive.ReactorHttpHandlerAdapter;
import org.springframework.web.reactive.function.server.HandlerFunction;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.ipc.netty.http.server.HttpServer;
import java.io.IOException;
import static org.springframework.web.reactive.function.BodyInserters.fromObject;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;

public class FunctionalWebApplication {

    public static AllUsers allUsers = new AllUsers();
    public static Token token = new Token();

    static RouterFunction getRouter() {
        return route(POST("/signup"), req ->
                    req
                   .bodyToMono(User.class)
                   .doOnSuccess(user -> allUsers.addUser(user))
                   .map(user -> ok().body(fromObject("User saved")))
                   .block())
                   .andRoute(POST("/signin"), req ->
                    req
                   .bodyToMono(User.class)
                   .map(user -> {
                       User checkedUser = allUsers.checkUser(user);
                       String us;
                       if (checkedUser == null)
                            us = "idi na xuy";
                       else
                            us = token.getUuid(user);
                       return ok().body(fromObject(us));})
                   .block()).andRoute(POST("/hello"), req ->
                        req
                                .bodyToMono(String.class)
                                .map(user -> {
                                    //User checkedUser = allUsers.checkUser(user);
                                    String us;
                                    if (token.checkUiid(user))
                                        us = "Hello";
                                    else
                                        us = "idi na fig";
                                    return ok().body(fromObject(us));})
                                .block());
    }

    public static void main(String[] args) throws IOException, LifecycleException, InterruptedException {

        RouterFunction router = getRouter();

        HttpHandler httpHandler = RouterFunctions.toHttpHandler(router);

        HttpServer
                .create("localhost", 8080)
                .newHandler(new ReactorHttpHandlerAdapter(httpHandler))
                .block();

        Thread.currentThread().join();
    }
}
