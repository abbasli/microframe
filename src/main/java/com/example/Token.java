package com.example;

import com.sun.org.apache.xpath.internal.operations.Bool;
import javafx.util.Pair;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class Token {

    private Map<String, User> myToken = new HashMap<>();

    public String getUuid(User user) {

        if (myToken.containsValue(user)) {
            Set<Map.Entry<String, User>> set = myToken.entrySet();
            for (Map.Entry<String, User> entry : set) {
                if(entry.getValue().equals(user)) {
                    return entry.getKey();
                }
            }
        }
        String hash = UUID.randomUUID().toString().replace("-", "");
        myToken.put(hash, user);
        return hash;
    }

    public Boolean checkUiid(String uiid) {
            return myToken.containsKey(uiid);
    }
}
